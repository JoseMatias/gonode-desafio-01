const express = require('express');
const nunjucks = require('nunjucks');
const path = require('path');
const bodyParser = require('body-parser');
const moment = require('moment');

const app = express();

const checkMiddleware = (req, res, next) => {
  if (!req.query.name)
    return res.redirect('/');

  next();
};

nunjucks.configure('views', {
  autoescape: true,
  express: app,
  watch: true,
});

app.set('view engine', 'njk');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded({
  extended: false,
}));

app.get('/', (req, res) => {
  res.render('main');
});

app.post('/check', (req, res) => {
  const { name, birthDate } = req.body;

  if (!name || !birthDate)
    return res.redirect('/');

  const testDate = moment().subtract(18, 'year').format('YYYY-MM-DD');
  const isMajor = moment(birthDate).isBefore(testDate);

  if (isMajor)
    return res.redirect('/major?name='.concat(name));
  else
    return res.redirect('/minor?name='.concat(name));
});

app.get('/major', checkMiddleware, (req, res) => {
  return res.render('major', { name: req.query.name });
});

app.get('/minor', checkMiddleware, (req, res) => {
  return res.render('minor', { name: req.query.name });
});

app.listen(3000);
